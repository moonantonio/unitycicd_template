#!/usr/bin/env bash

function parse_semver() {
    local token="$1"
    local major=0
    local minor=0
    local patch=0

    if egrep '^[0-9]+\.[0-9]+\.[0-9]+' <<<"$token" >/dev/null 2>&1 ; then
        # It has the correct syntax.
        local n=${token//[!0-9]/ }
        local a=(${n//\./ })
        major=${a[0]}
        minor=${a[1]}
        patch=${a[2]}
    fi
    
    echo "$major $minor $patch"
}

version=$(cat ./ProjectSettings/ProjectSettings.asset | grep "bundleVersion:.*" | awk '{ print $2}')
a=($(parse_semver "$version"))
major=${a[0]}
minor=${a[1]}
patch=${a[2]}
printf "Old version : $major.$minor.$patch\n"

if [[ $CI_COMMIT_MESSAGE =~ major ]] ; 
then
    patch=0;
    minor=0;
    major=$((major+1));
elif [[ $CI_COMMIT_MESSAGE =~ minor ]] ;
then
    patch=0;
    minor=$((minor+1));
else
    patch=$((patch+1));
fi
perl -pi -e "s/bundleVersion:.*/bundleVersion: $major.$minor.$patch/g" ./ProjectSettings/ProjectSettings.asset
printf "New version : $major.$minor.$patch\n"