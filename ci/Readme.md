# Manual Commands

## Tests
```
MANUAL playmode
```
```
MANUAL editmode
```

## Builds
```
MANUAL StandaloneLinux64
```
```
MANUAL StandaloneWindows64
```

## Request delete gobs
```
MANUAL delete-jobs
PER_PAGE nb
SORT asc or dsc
```
