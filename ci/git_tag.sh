#!/usr/bin/env bash

git config --global user.email "marc.cerutti@outlook.fr"
git config --global user.name "Gitlab CI"
git add ./ProjectSettings
git commit -m "$PROJECT_VERSION" 
git push "https://${GITLAB_USER_LOGIN}:${OAUTH2_TOKEN}@${CI_REPOSITORY_URL#*@}" "HEAD:${CI_COMMIT_REF_NAME}" -o ci.skip

if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]
then
    git tag "$PROJECT_VERSION"
    git push --tags "https://${GITLAB_USER_LOGIN}:${OAUTH2_TOKEN}@${CI_REPOSITORY_URL#*@}" "HEAD:${CI_COMMIT_REF_NAME}"
fi