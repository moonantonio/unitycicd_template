namespace m21cerutti.BUS.Tests.Runtime {
	using System;
	using System.Collections;
	using System.IO;

	using NUnit.Framework;

	using UnityEditor;
	using UnityEditor.SceneManagement;

	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityEngine.TestTools;

	public class PlayTests {
		private string[] m_pathsScenes;

		[OneTimeSetUp]
		public void Init() {
			string[] guids = AssetDatabase.FindAssets("t:Scene", new string[] {"Assets/m21cerutti"});
			m_pathsScenes = Array.ConvertAll(guids, AssetDatabase.GUIDToAssetPath);
			m_pathsScenes = Array.FindAll(m_pathsScenes, File.Exists);
		}

		[UnitySetUp] public void BeforeTest() { }

		[UnityTearDown] public void AfterTest() { }

		[OneTimeTearDown] public void End() { }

		[UnityTest]
		public IEnumerator PlayAllScenes() {
			// Remove start scene test
			AssetDatabase.DeleteAsset(SceneManager.GetActiveScene().path);
			// Test loading of all scenes
			foreach (string path in m_pathsScenes) {
				Debug.Log("### Test Scene " + path);

				yield return EditorSceneManager.LoadSceneAsyncInPlayMode(
					path, new LoadSceneParameters(LoadSceneMode.Single));
				yield return new WaitForSeconds(2.0f);
				Debug.Log("### EndTest Scene " + path);
			}
			Debug.Log("EndTest PlayAllScenes.");
		}
	}
}