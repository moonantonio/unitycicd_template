namespace m21cerutti.BUS.Editor {
	using System.IO;
	using System.IO.Compression;

	using UnityEditor;
	using UnityEditor.Callbacks;

	using UnityEngine;

	public static class ZipAfterBuild {
		[PostProcessBuild(1)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
			Directory.CreateDirectory("Build/");
			string path_folder = Path.GetDirectoryName(pathToBuiltProject);
			Debug.Log("Zip created from "+path_folder);
			string zip_file = "Build/" + Application.productName + '_' + EditorUserBuildSettings.activeBuildTarget +
							 '_' + Application.version + ".zip";
			zip_file = zip_file.Replace(" ", "");
			ZipFile.CreateFromDirectory(path_folder, zip_file);
		}
	}
}