﻿namespace m21cerutti.BUS.Editor {
	using UnityEngine;

	namespace m21cerutti.ClapClapEvent.Runtime {
		[System.Serializable]
		public struct GitPackage {
			public string m_name;
			public string m_url;
			public bool m_enabled;
		}
		
		[ExecuteAlways]
		public class PackageList : ScriptableObject {
			[SerializeField] public GitPackage[] m_packages;
		}
	}
}