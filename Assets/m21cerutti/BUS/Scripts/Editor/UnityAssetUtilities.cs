namespace m21cerutti.BUS.Editor {
	using System.Collections.Generic;
	using System.IO;

	using UnityEditor;

	using UnityEngine;

	public static class UnityAssetUtilities {
		public static string GetActualPathMenuItem() {
			string path = AssetDatabase.GetAssetPath(Selection.activeObject);
			if (path == "") {
				path = "Assets";
			} else if (Path.GetExtension(path) != "") {
				path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
			}

			return path;
		}

		[MenuItem("Assets/DisplayAssetsInfo")]
		public static void DisplayAssetsInfos(MenuCommand command) {
			foreach (string guid in Selection.assetGUIDs) {
				string asset_path = AssetDatabase.GUIDToAssetPath(guid);
				long file;
				Object obj = AssetDatabase.LoadMainAssetAtPath(asset_path);
				if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(obj, out _, out file)) {
					Debug.Log("Asset: " + obj.name + "\n"
							  + "Path: " + asset_path + "\n"
							  + "Type: " + obj.GetType() + "\n"
							  + "Instance ID: " + obj.GetInstanceID() + "\n"
							  + "GUID: " + guid + "\n"
							  + "File ID: " + file);
				}
			}
		}

		public static bool IsPackageInstalled(string packageId) {
			string manifest = Application.dataPath + "/../Packages/manifest.json";
			if (!File.Exists(manifest)) {
				return false;
			}

			string jsonText = File.ReadAllText(manifest);

			return jsonText.Contains(packageId);
		}

		//Based on : https://answers.unity.com/questions/321615/code-to-mimic-find-references-in-scene.html
		/// <summary>
		///     Get a list of Monobehaviour that reference a ScriptableObject.
		///     Heavy function, don't call it every frames.
		/// </summary>
		/// <param name="to">ScriptableObject</param>
		/// <returns>A List of MonoBehaviour.</returns>
		public static List<MonoBehaviour> GetListSoReferencesInScene(ScriptableObject to) {
			List<MonoBehaviour> referenced_by = new List<MonoBehaviour>();
			string to_name = $"{to.name}.{to.GetType().Name}";
			MonoBehaviour[] list_all = (MonoBehaviour[]) Object.FindObjectsOfType(typeof(MonoBehaviour));
			foreach (MonoBehaviour component in list_all) {
				SerializedObject so = new SerializedObject(component);
				SerializedProperty sp = so.GetIterator();
				while (sp.NextVisible(true)) {
					if (sp.propertyType == SerializedPropertyType.ObjectReference && sp.objectReferenceValue == to
						&& component is MonoBehaviour behaviour) {
						referenced_by.Add(behaviour);
					}
				}
			}

			return referenced_by;
		}
	}
}