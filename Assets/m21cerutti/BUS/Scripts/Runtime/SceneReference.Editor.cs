﻿#if UNITY_EDITOR

namespace m21cerutti.BUS.Runtime {
    
    // Based on JohannesMP (2018-08-12), 2019 S. Tarık Çetin, Tymski, MIT license
    //
    // Editor part with serialization/deserialization.

    using UnityEditor;
    using UnityEditor.SceneManagement;

    using UnityEngine;
    using UnityEngine.SceneManagement;

    /// <summary>
    /// Editor part with serialization/deserialization.
    /// </summary>
    public partial class SceneReference : ISerializationCallbackReceiver
    {
        private bool isValidSceneAsset
        {
            get
            {
                if (!m_sceneAsset) return false;

                return m_sceneAsset is SceneAsset;
            }
        }

        // Called to prepare this data for serialization. Stubbed out when not in editor.
        public void OnBeforeSerialize()
        {
            HandleBeforeSerialize();
        }

        // Called to set up data for deserialization. Stubbed out when not in editor.
        public void OnAfterDeserialize()
        {
            // We sadly cannot touch assetdatabase during serialization, so defer by a bit.
            EditorApplication.update += HandleAfterDeserialize;
        }

        private void HandleBeforeSerialize()
        {
            // Asset is invalid but have Path to try and recover from
            if (isValidSceneAsset == false && string.IsNullOrEmpty(m_scenePath) == false)
            {
                m_sceneAsset = GetSceneAssetFromPath();
                if (m_sceneAsset == null) m_scenePath = string.Empty;

                EditorSceneManager.MarkAllScenesDirty();
            }
            // Asset takes precendence and overwrites Path
            else
            {
                m_scenePath = GetScenePathFromAsset();
            }

            m_buildIndex = SceneUtility.GetBuildIndexByScenePath(m_scenePath);
        }

        private void HandleAfterDeserialize()
        {
            EditorApplication.update -= HandleAfterDeserialize;
            // Asset is valid, don't do anything - Path will always be set based on it when it matters
            if (isValidSceneAsset) return;

            // Asset is invalid but have path to try and recover from
            if (string.IsNullOrEmpty(m_scenePath)) return;

            m_sceneAsset = GetSceneAssetFromPath();
            // No asset found, path was invalid. Make sure we don't carry over the old invalid path
            if (!m_sceneAsset)
            {
                m_scenePath = string.Empty;
                m_buildIndex = -1;
            }

            if (!Application.isPlaying) EditorSceneManager.MarkAllScenesDirty();
        }
    }
}

#endif