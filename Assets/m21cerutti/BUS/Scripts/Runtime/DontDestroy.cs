namespace m21cerutti.BUS.Runtime {
    using UnityEngine;

    public class DontDestroy : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
