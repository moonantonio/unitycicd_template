namespace m21cerutti.ClapClapEvent.Editor {
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text;

	using BUS.Editor;

	using Runtime;

	using UnityEditor;

	using UnityEngine;

	using Object = UnityEngine.Object;

	/// <summary>
	///     Permit to visualize all informations about an event.
	/// </summary>
	[Serializable]
	public class EventVisualiser : ISerializationCallbackReceiver {
		#region _NameCosmetic
		[SerializeField, HideInInspector] private string m_name;
		#endregion

		[SerializeField] public EventClapClap m_ev;
		[SerializeField] public MonoBehaviour[] m_listeners;
		[SerializeField] public MonoBehaviour[] m_callers;

		#region _NameCosmeticMethods
		void ISerializationCallbackReceiver.OnBeforeSerialize() {
			m_name = m_ev != null ? m_ev.GetEventName() : "Null";
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize() { }
		#endregion
	}

	/// <summary>
	///     Manger editor that permit to simplify the manipulation of events and recievers.
	/// </summary>
	public class ManagerEventEditorWindow : EditorWindow {
		private const string defaultFolderEvents = "/ProjectEvents";
		private const string folderCustomEventTypes = "/m21cerutti/ClapClapEvent/Scripts/CustomEvents";

		[SerializeField] private EventClapClap[] m_allEvents;
		[SerializeField] private EventClapClap[] m_excludedEvents = { };
		[SerializeField] private EventVisualiser[] m_referencedInScene;
		[SerializeField] private EventClapClap[] m_notUsedInScene;
		[SerializeField] private EventClapClap m_selected;
		[SerializeField] private EventClapClap m_selectedEventToModify;

		[SerializeField] private DebugEventValue m_debugLevel;
		private readonly float m_spaceUiUtilitiesButton = 175;

		private readonly string[] m_templates = {
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEvent-ComplexEvent.cs.txt",
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEventInspector-ComplexEventInspector.cs.txt",
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEventReceiver-ComplexEventReceiver.cs.txt",
			"Assets/ScriptTemplates/82-CustomScript__TypeEvents__ComplexEventReceiverInspector-ComplexEventReceiverInspector.cs.txt"
		};

		private bool m_advancedPanel;
		private string m_eventNameCreation = "Complex";
		private Type[] m_eventTypes = { };
		private SerializedProperty m_propEventsExcluded;
		private SerializedProperty m_propEventsNotUsedInScene;
		private SerializedProperty m_propEventsReferencedInScene;
		private Type[] m_receiverTypes = { };

		private Vector2 m_scrollPos;

		private int m_selectedTypeCreation;
		private int m_selectedTypeModification;

		private SerializedObject m_soThis;
		private bool m_toggle = true;
		private bool m_toggleEventInspector;
		private bool m_toggleReceiver = true;
		private bool m_toggleReceiverInspector;

		private void OnEnable() {
			ScriptableObject target = this;
			m_soThis = new SerializedObject(target);
			m_propEventsExcluded = m_soThis.FindProperty("m_excludedEvents");
			m_propEventsReferencedInScene = m_soThis.FindProperty("m_referencedInScene");
			m_propEventsNotUsedInScene = m_soThis.FindProperty("m_notUsedInScene");

			m_debugLevel =
				AssetDatabase.LoadAssetAtPath<DebugEventValue>(
					AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("t:DebugEventValue")[0]));

			// Used fo initialisation
			Directory.CreateDirectory(Application.dataPath +defaultFolderEvents);

			UpdateTypes();
		}

		private void OnGUI() {
			EditorGUIUtility.labelWidth = 75.0f;
			EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
			m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
			EditorGUILayout.Separator();
			GUILayout.Label("ClapClapEvent Manager", EditorStyles.whiteLargeLabel);
			EditorGUILayout.Separator();
			FuncEditor.DrawUILine(Color.gray);

			//Exclude events
			EditorGUILayout.PropertyField(m_propEventsExcluded, true);
			m_soThis.ApplyModifiedProperties();
			EditorGUILayout.Separator();

			// Refresh
			GUI.enabled = !EditorApplication.isPlayingOrWillChangePlaymode || EditorApplication.isPaused;
			if (GUILayout.Button("Refresh project's events")) {
				RefreshEventsProjects();
			}
			EditorGUILayout.Separator();

			//Display
			GUI.enabled = true;
			if (m_referencedInScene == null && m_notUsedInScene == null) {
				EditorGUILayout.LabelField("Refresh to get events.");
			}

			if (m_referencedInScene != null) {
				EditorGUILayout.PropertyField(m_propEventsReferencedInScene, true);
				m_soThis.Update();
			}

			if (m_notUsedInScene != null) {
				EditorGUILayout.PropertyField(m_propEventsNotUsedInScene, true);
				m_soThis.Update();
			}

			EditorGUILayout.Separator();
			GUILayout.Label("Utilities", EditorStyles.centeredGreyMiniLabel);
			EditorGUILayout.Separator();

			//Create event
			EditorGUILayout.BeginHorizontal();
			string[] types_names = m_eventTypes.Select(type => type.Name).ToArray();
			EditorGUILayout.LabelField("TypeEvent", GUILayout.ExpandWidth(false));
			m_selectedTypeCreation = EditorGUILayout.Popup(m_selectedTypeCreation, types_names);

			if (GUILayout.Button("Create event", GUILayout.Width(m_spaceUiUtilitiesButton))) {
				Object asset = CreateInstance(m_eventTypes[m_selectedTypeCreation]);
				string folder_path_events =
					EditorUtility.SaveFilePanel("Folder of events",
												Application.dataPath + defaultFolderEvents,
												m_eventTypes[m_selectedTypeCreation].Name, "asset");
				if (folder_path_events != "") {
					string asset_path =
						AssetDatabase.GenerateUniqueAssetPath(
							folder_path_events.Replace(Application.dataPath, "Assets"));
					AssetDatabase.CreateAsset(asset, asset_path);
					AssetDatabase.SaveAssets();
					Selection.activeObject = asset;
					m_selected = asset as EventClapClap;
					Undo.RegisterCreatedObjectUndo(m_selected, "Add event");
				}
			}
			EditorGUILayout.EndHorizontal();
			FuncEditor.DrawUILine(Color.gray, 1);

			// Add listener
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Event", GUILayout.ExpandWidth(false));
			m_selected =
				(EventClapClap) EditorGUILayout.ObjectField(m_selected, typeof(EventClapClap), false);
			GUI.enabled = !(m_selected == null || Selection.activeGameObject == null);

			if (GUILayout.Button("Add listener to selected", GUILayout.Width(m_spaceUiUtilitiesButton))) {
				ClapClapEditorUtilities.CallAddEventToListenerUnsafe(m_selected, Selection.activeGameObject);
				Undo.RecordObject(m_selected, "Add listener");
			}
			GUI.enabled = true;

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Space(30);

			m_advancedPanel = EditorGUILayout.Foldout(m_advancedPanel, "Advanced", EditorStyles.foldoutHeader);
			if (m_advancedPanel) {
				//Debug
				EditorGUILayout.Separator();
				GUILayout.Label("Debug", EditorStyles.centeredGreyMiniLabel);
				EditorGUILayout.Separator();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("DebugLevel");
				m_debugLevel.debugValue = (DebugLevel) EditorGUILayout.EnumPopup(m_debugLevel.debugValue);
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.Separator();

				GUILayout.Label("DebugColors");
				EditorGUILayout.Separator();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Event log", EditorStyles.miniLabel);
				m_debugLevel.debugColors[0] =
					EditorGUILayout.ColorField(m_debugLevel.debugColors[0], GUILayout.Width(m_spaceUiUtilitiesButton));
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Receiver log", EditorStyles.miniLabel);
				m_debugLevel.debugColors[1] =
					EditorGUILayout.ColorField(m_debugLevel.debugColors[1], GUILayout.Width(m_spaceUiUtilitiesButton));
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Other log", EditorStyles.miniLabel);
				m_debugLevel.debugColors[2] =
					EditorGUILayout.ColorField(m_debugLevel.debugColors[2], GUILayout.Width(m_spaceUiUtilitiesButton));
				EditorGUILayout.EndHorizontal();

				// Create type
				EditorGUILayout.Separator();
				GUILayout.Label("Create typed event", EditorStyles.centeredGreyMiniLabel);
				EditorGUILayout.Separator();

				EditorGUIUtility.labelWidth = m_spaceUiUtilitiesButton;
				m_eventNameCreation = EditorGUILayout.TextField("Type", m_eventNameCreation);
				m_toggle = EditorGUILayout.Toggle("Event", m_toggle, GUILayout.Width(m_spaceUiUtilitiesButton));
				m_toggleEventInspector =
					EditorGUILayout.Toggle("Inspector", m_toggleEventInspector,
										   GUILayout.Width(m_spaceUiUtilitiesButton));
				m_toggleReceiver =
					EditorGUILayout.Toggle("Receiver", m_toggleReceiver, GUILayout.Width(m_spaceUiUtilitiesButton));
				m_toggleReceiverInspector = EditorGUILayout.Toggle("ReceiverInspector", m_toggleReceiverInspector,
																   GUILayout.Width(m_spaceUiUtilitiesButton));
				if (GUILayout.Button("Create TypedEvent")) {
					string folder_path_typeevents =
						EditorUtility.OpenFolderPanel("Folder of event's types",
													  Application.dataPath + folderCustomEventTypes, "");
					if (folder_path_typeevents != "") {
						if (m_toggle) {
							ScriptTemplatesProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, m_eventNameCreation + "Event.cs"), m_templates[0]);
						}
						if (m_toggleEventInspector) {
							ScriptTemplatesProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, "Editor/",
											 m_eventNameCreation + "EventInspector.cs"),
								m_templates[1]);
						}
						if (m_toggleReceiver) {
							ScriptTemplatesProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, m_eventNameCreation + "EventReceiver.cs"),
								m_templates[2]);
						}
						if (m_toggleReceiverInspector) {
							ScriptTemplatesProcessor.ScriptCreationFromTemplate(
								Path.Combine(folder_path_typeevents, "Editor/",
											 m_eventNameCreation + "EventReceiverInspector.cs"), m_templates[3]);
						}
					}
				}

				//Change existing event type
				EditorGUILayout.Separator();
				GUILayout.Label("Change existing event type", EditorStyles.centeredGreyMiniLabel);
				EditorGUILayout.Separator();

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("New TypeEvent", GUILayout.ExpandWidth(false));
				m_selectedTypeModification = EditorGUILayout.Popup(m_selectedTypeModification, types_names);
				EditorGUILayout.EndHorizontal();

				m_selectedEventToModify =
					(EventClapClap) EditorGUILayout.ObjectField(m_selectedEventToModify, typeof(EventClapClap), false);
				EditorGUILayout.Separator();
				GUI.enabled = !(m_selectedEventToModify == null);
				if (GUILayout.Button("Change Type Event")) {
					Type type_dest = m_eventTypes[m_selectedTypeModification];
					ChangeExistingEventType(m_selectedEventToModify, type_dest);
				}
				GUI.enabled = true;
			}
			EditorGUILayout.Space(30);

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndVertical();
		}

		private void RefreshEventsProjects() {
			UpdateTypes();

			m_referencedInScene = null;
			m_notUsedInScene = null;

			List<EventVisualiser> events_used = new List<EventVisualiser>(m_allEvents.Length);
			List<EventClapClap> events_not_used = new List<EventClapClap>(m_allEvents.Length);
			float progress = 0;
			foreach (EventClapClap ev in m_allEvents.Except(m_excludedEvents)) {
				progress++;
				EditorUtility.DisplayProgressBar("Processing " + m_allEvents.Length + " events...",
												 "Getting " + ev.name + " references in scene.",
												 progress / m_allEvents.Length);
				List<MonoBehaviour> all_references = UnityAssetUtilities.GetListSoReferencesInScene(ev);
				ILookup<bool, MonoBehaviour> is_listener = all_references.ToLookup(
					script => script.GetType().IsSubclassOf(typeof(EventReceiver)));
				if (is_listener[true].Count() != 0 || is_listener[false].Count() != 0) {
					events_used.Add(new EventVisualiser {
						m_ev = ev,
						m_listeners = is_listener[true].ToArray(),
						m_callers = is_listener[false].ToArray()
					});
				} else {
					events_not_used.Add(ev);
				}
			}
			m_referencedInScene = events_used.ToArray();
			m_notUsedInScene = events_not_used.ToArray();
			m_soThis.Update();
			Repaint();
			EditorUtility.ClearProgressBar();
		}

		private void UpdateTypes() {
			string[] guids = AssetDatabase.FindAssets("t:EventClapClap");
			m_allEvents =
				guids.Select(guid => AssetDatabase.LoadAssetAtPath<EventClapClap>(AssetDatabase.GUIDToAssetPath(guid)))
					 .ToArray();

			m_eventTypes = ReflectionUtilities.GetInheritedClasses(typeof(EventClapClap));
			m_receiverTypes = ReflectionUtilities.GetInheritedClasses(typeof(EventReceiver));
		}

		private void ChangeExistingEventType(EventClapClap ev, Type destination) {
			Undo.IncrementCurrentGroup();
			Undo.SetCurrentGroupName("Change event type");
			int undo_group_index = Undo.GetCurrentGroup();

			// List old references
			StringBuilder list_references = new StringBuilder();
			list_references.Append("References impacted list see below\n");

			List<MonoBehaviour> all_references = UnityAssetUtilities.GetListSoReferencesInScene(ev);
			ILookup<bool, MonoBehaviour> is_listener = all_references.ToLookup(
				script => script.GetType().IsSubclassOf(typeof(EventReceiver)));

			list_references.Append("Listeners :\n");
			foreach (MonoBehaviour listener in is_listener[true]) {
				list_references.Append(listener.gameObject.name + " (" + listener.GetType().Name + ")" + "\n");
				Undo.RegisterCompleteObjectUndo(listener, listener.name);
			}
			list_references.Append("\n");

			list_references.Append("Callers :\n");
			foreach (MonoBehaviour caller in is_listener[false]) {
				list_references.Append(caller.gameObject.name + " (" + caller.GetType().Name + ")" + "\n");
				Undo.RegisterCompleteObjectUndo(caller.gameObject, caller.name);
			}
			Debug.Log(list_references.ToString());

			// Replace guids for altering types
			//TODO Improve exact search
			string old_guid = AssetDatabase.FindAssets("t:Script " + ev.GetType().Name)
										   .Where(guid =>
													  AssetDatabase.GUIDToAssetPath(guid)
																   .EndsWith(ev.GetType().Name + ".cs"))
										   .ToArray()[0];
			string new_guid = AssetDatabase.FindAssets("t:Script " + destination.Name)
										   .Where(guid =>
													  AssetDatabase.GUIDToAssetPath(guid)
																   .EndsWith(destination.Name + ".cs"))
										   .ToArray()[0];

			string event_asset = AssetDatabase.GetAssetPath(ev);
			string text = File.ReadAllText(event_asset);
			text = text.Replace(old_guid, new_guid);
			File.WriteAllText(event_asset, text);
			Undo.RegisterCompleteObjectUndo(ev, ev.name);
			Undo.CollapseUndoOperations(undo_group_index);
			Debug.Log("Change " + ev.name + " from " + ev.GetType().Name + " to " + destination.Name);
			AssetDatabase.ImportAsset(event_asset);

			ev = AssetDatabase.LoadAssetAtPath<EventClapClap>(event_asset);
			m_selectedEventToModify = ev;

			// Add receiver for keeping references
			foreach (MonoBehaviour listener in is_listener[true]) {
				ClapClapEditorUtilities.CallAddEventToListenerUnsafe(ev, listener.gameObject);
			}
		}

		[MenuItem("Tools/ClapClapEvent/ManagerEditor", false, 0)]
		public static void ShowWindow() {
			EditorWindow win = GetWindow(typeof(ManagerEventEditorWindow));
		}
	}
}