namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public class Vector3EventReceiver : MultiParamEventReceiver<Vector3> { }
}