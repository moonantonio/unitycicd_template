namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	public delegate void Vector2EventCallback(Vector2 param);

	[CreateAssetMenu(fileName = "Vector2Event", menuName = "Events/Vector2Event")]
	public class Vector2Event : ParametrisedEvent<Vector2> { }
}