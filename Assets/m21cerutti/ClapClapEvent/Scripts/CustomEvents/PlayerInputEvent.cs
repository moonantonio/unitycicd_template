#if USE_NEWINPUT
namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;
	using UnityEngine.InputSystem;

	[CreateAssetMenu(fileName = "PlayerInputEvent", menuName = "Events/PlayerInputEvent")]
	public class PlayerInputEvent : ParametrisedEvent<PlayerInput> { }
}
#endif