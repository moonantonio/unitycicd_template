﻿namespace m21cerutti.ClapClapEvent.CustomEvents {
	using Runtime;

	using UnityEngine;

	[CreateAssetMenu(fileName = "BoolEvent", menuName = "Events/BoolEvent")]
	public class BoolEvent : ParametrisedEvent<bool> { }
}