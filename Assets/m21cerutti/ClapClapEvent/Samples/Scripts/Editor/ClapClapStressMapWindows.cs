namespace m21cerutti.ClapClapEvent.Samples.Editor {
	using System.Collections.Generic;

	using BUS.Editor;
	using BUS.Runtime;

	using ClapClapEvent.Editor;

	using CustomEvents;

	using Runtime;

	using UnityEditor;

	using UnityEngine;

	public class ClapClapStressMapWindows : EditorWindow {
		[SerializeField] private GameObject m_map;
		[SerializeField] private int m_levelsNumber = 1;
		[SerializeField] private int m_objectsByLevel = 1;
		[SerializeField, ReadOnlyField] private int m_numberOfObjects;

		[SerializeField] private EventClapClap[] m_testEvents;
		[SerializeField] private float m_probabilityEventExistence = 0.5f;
		[SerializeField] private AnimationCurve m_probabilityEvents;
		private Editor m_editorWin;
		private Vector2 m_scrollPos;

		private SerializedObject m_soThis;

		private void OnEnable() {
			ScriptableObject target = this;
			m_soThis = new SerializedObject(target);
			m_editorWin = Editor.CreateEditor(this);
		}

		private void OnGUI() {
			EditorGUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
			m_scrollPos = EditorGUILayout.BeginScrollView(m_scrollPos);
			EditorGUILayout.Separator();
			GUILayout.Label("Create Stress Map", EditorStyles.whiteLargeLabel);
			EditorGUILayout.Separator();
			FuncEditor.DrawUILine(Color.gray);

			m_editorWin.DrawDefaultInspector();

			GUI.enabled = !EditorApplication.isPlayingOrWillChangePlaymode || EditorApplication.isPaused;
			if (GUILayout.Button("Create")) {
				CreateStressMap();
			}
			GUI.enabled = true;
			EditorGUILayout.Space(30);

			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndVertical();
		}

		[MenuItem("Tools/ClapClapEvent/Samples/CreateStressMap")]
		public static void ShowWindow() {
			EditorWindow win = GetWindow(typeof(ClapClapStressMapWindows));
		}

		private void CreateStressMap() {
			if (m_map.transform.childCount > 0) {
				DestroyImmediate(m_map.transform.GetChild(0).gameObject);
				m_numberOfObjects = 0;
			}
			GameObject root = new GameObject {
				name = "root",
				transform = {
					parent = m_map.transform
				}
			};
			Queue<KeyValuePair<GameObject, int>> levels_to_create = new Queue<KeyValuePair<GameObject, int>>();
			levels_to_create.Enqueue(new KeyValuePair<GameObject, int>(root, 1));

			int count = 0;
			float all = Mathf.Pow(m_objectsByLevel, m_levelsNumber);
			while (levels_to_create.Count > 0) {
				if (EditorUtility.DisplayCancelableProgressBar("Creation map", "Creating " + all + " objects.",
															   count / all)) {
					break;
				}
				KeyValuePair<GameObject, int> node = levels_to_create.Dequeue();
				count++;
				int level = node.Value;
				if (level >= m_levelsNumber + 1) {
					continue;
				}
				for (int i = 0; i < m_objectsByLevel; i++) {
					GameObject new_node = new GameObject {
						name = node.Key.transform.name + "_" + i,
						transform = {
							position = new Vector3().Random(100.0f),
							parent = node.Key.transform
						}
					};

					new_node.AddComponent<BoxCollider>();
					new_node.AddComponent<IntEventReceiver>();
					new_node.AddComponent<BoolEventReceiver>();
					// Add aleatory events
					if (Random.value > m_probabilityEventExistence && m_probabilityEvents.length > 0) {
						int ran = (int) (m_probabilityEvents.Evaluate(Random.value) * m_testEvents.Length);
						EventClapClap ev = m_testEvents[ran];
						ClapClapEditorUtilities.CallAddEventToListenerUnsafe(ev, new_node);
					}

					levels_to_create.Enqueue(new KeyValuePair<GameObject, int>(new_node, level + 1));
					m_numberOfObjects++;
				}
			}
			EditorUtility.ClearProgressBar();
		}
	}
}